# Royal Tyrrell Museum Desktop App

A desktop application used in touch screens for visitants.

Download link: https://s3.amazonaws.com/tyrrell-museum-app-76523/Royal+Tyrrell+Museum+App+Setup+APP_VERSION.exe

You can find latest APP_VERSION in the [electron package.json](/electron/package.json)
