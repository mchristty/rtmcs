# Electron App

Backend for the application UI, it is only a wrapper for the UI.

## Setting up S3 release bucket

- Create a bucket in S3.
- Disable all public restrictions in the bucket/permissions/public access settings (all flags to false).
- Create credentials and put it in the shared file credentials for AWS, see [here](https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/loading-node-credentials-shared.html).
- Attach this policy, you should replace `bucket-name` with the real bucket name in the JSON below.
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowAppS3Releases",
            "Effect": "Allow",
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:GetObjectVersion",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::bucket-name/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads"
            ],
            "Resource": [
                "arn:aws:s3:::bucket-name"
            ]
        }
    ]
}
```
- Now you can use the autoupdate feature and releasing to S3.
