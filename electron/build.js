// build script

const shell = require('shelljs')

shell.cd('..')

shell.cd('ui')

shell.exec('npm run build', () => {
  shell.cp('-Rf', 'build/*', '../electron/compiled-ui')
})
