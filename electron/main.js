const { app, BrowserWindow } = require('electron')
const isDev = require('electron-is-dev')
const autoUpdater = require('./autoUpdater')
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';
let win

function createWindow () {
  win = new BrowserWindow({ width: 800, height: 600 ,backgroundColor: "#FFFFFF"})

  // win.setFullScreen(true)
  win.webContents.openDevTools()
  win.loadURL('http://localhost:3000')
  // if (isDev) {
  //   win.webContents.openDevTools()
  //   win.loadURL('http://localhost:3000')
  // } else {
  //   autoUpdater.checkForUpdatesAndNotify()
  //   win.loadFile('compiled-ui/index.html')
  // } 

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})
