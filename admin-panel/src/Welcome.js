import React from 'react'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import styled from 'styled-components'

export default () => (
  <div>
    <p>Welcome to the RTMCS admin panel, here you can modify:</p>
    <StyledLink to="/people">
      <Button variant="contained">Donors & Executive Members</Button>
    </StyledLink>
    <br />
    <br />
    <StyledLink to="/questions">
      <Button variant="contained" >Dinosaur Questions</Button>
    </StyledLink>
    <br />
    <br />
    <StyledLink to="/shop">
      <Button variant="contained" >Museum shop</Button>
    </StyledLink>
    <br />
    <br />
    <StyledLink to="/fellowship">
      <Button variant="contained" >Post Doctoral Fellowship</Button>
    </StyledLink>
  </div>
)

const StyledLink = styled(Link)`
  text-decoration: none;
`
