import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import * as api from './api'

const styles = {
  textField: {
    marginTop: '5px',
    marginBottom: '5px',
  }
}

class Login extends React.Component {

  state = {
    username: '',
    password: '',
  }

  handleChange = fieldName => ev => {
    this.setState({ [fieldName]: ev.target.value })
  }

  login = ev => {
    ev.preventDefault()
    if (!this.state.username || !this.state.password) {
      return alert('Please write your credentials')
    }
    api.login(this.state)
      .then(() => this.props.history.push('/'))
      .catch(err => alert(err === 401 ? 'Wrong username or password' : 'error with the server connection, maybe you don\'t have internet, if you have internet and the problem persists, please contact us to provide support'))
  }

  render() {
    const { classes } = this.props

    return (
      <Container onSubmit={this.login}>
        <Typography variant="h5" color="primary" style={{ marginBottom: '14px' }}>
          Admin Login
        </Typography>
        <TextField
          label="Username"
          className={classes.textField}
          value={this.state.username}
          onChange={this.handleChange('username')}
        />
        <TextField
          label="Password"
          type="password"
          className={classes.textField}
          value={this.state.password}
          onChange={this.handleChange('password')}
        />
        <Button
          type="submit"
          style={{ marginTop: '15px' }}
          color="secondary"
        >
          Open
        </Button>
      </Container>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
}

const Container = styled.form`
  padding-top: 35px;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export default withRouter(withStyles(styles)(Login))
