import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import sections from './sections'

const styles = theme => ({
  list: {
    width: 270,
  },
  fullList: {
    width: 'auto',
  },
  toolbar: theme.mixins.toolbar,
})

function TemporaryDrawer({ open, classes, onClose }) {
  return (
    <div>
      <Drawer open={open} onClose={onClose}>
        <div
          tabIndex={0}
          role="button"
        >
          <div className={classes.list}>
            <div className={classes.toolbar} />
            <Divider />
            <List>
              {
                sections.map(section => (
                  <StyledLink to={section.path} onClick={onClose}>
                    <ListItem button>
                      <ListItemIcon>{section.icon}</ListItemIcon>
                      <ListItemText primary={section.title} />
                    </ListItem>
                  </StyledLink>
                ))
              }
            </List>
          </div>
        </div>
      </Drawer>
    </div>
  )
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(TemporaryDrawer)

const StyledLink = styled(Link)`
  text-decoration: none;
`
