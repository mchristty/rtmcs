const TOKEN_LOCALSTORAGE_KEY = 'app-token'

const AWS_BUCKET = process.env.NODE_ENV === 'production'
  ? 'rtmcs' // TODO: put here the Jeffs bucket
  : 'rtmcs'

let state = {
  token: localStorage.getItem(TOKEN_LOCALSTORAGE_KEY),
}

const serverURL = 'http://localhost:3004'
// process.env.NODE_ENV === 
// 'production'
//   ? 'https://rtmcs-backend-8rr55krn4.now.sh'
//    : 'http://localhost:3004'//'https://rtmcs-backend-8rr55krn4.now.sh' // 'http://localhost:3004'

export const isAuth = () => !!state.token

const apiGet = path => fetch(`${serverURL}/${path}`, {
  headers: {
    Authorization: `Bearer ${state.token}`,
  },
})
  .then(res => {
    if (!res.ok) {
      throw res.status
    }
   
    return res
  })
  .then(r => r.json())
  .catch(err => {
    throw err
  })

const apiPost = (path, body) => fetch(`${serverURL}/${path}`, {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${state.token}`,
  },
  body: JSON.stringify(body),
})
  .then(res => {
    if (!res.ok) {
      throw res.status
    }
    return res
  })
  .then(r => r.json())
  .catch(err => {
    throw err
  })

  const apiDelete = path => fetch(`${serverURL}/${path}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${state.token}`,
    },
  })
    .then(res => {
      if (!res.ok) {
        throw res.status
      }
      return res
    })
    .then(r => r.json())
    .catch(err => {
      throw err
    })

export const commonErrorAlert = () => alert('Conection error or server error, verify your connection and if the problem persist, please contact support')

export const login = async ({ username, password }) => {
  try {
    return await apiPost('login', { username, password })
      .then(res => {
        localStorage.setItem(TOKEN_LOCALSTORAGE_KEY, res.token)
        state.token = res.token
      })
  } catch(errCode) {
    throw errCode
  }
}

export const logout = () => {
  localStorage.removeItem(TOKEN_LOCALSTORAGE_KEY)
  state.token = ''
}

export const checkAuth = async () => {
  try {
    return await apiGet('api/checkToken')
  } catch(errCode) {
    throw errCode
  }
}

export const refreshToken = async () => {
  try {
    const res = await apiGet('api/refreshToken').then(r => r.json)
    state.token = res.token
    localStorage.setItem(TOKEN_LOCALSTORAGE_KEY, res.token)
    return res
  } catch(errCode) {
    throw errCode
  }
}

export const getPeople = async () => {
  try {
    return await apiGet('api/people')
  } catch(errCode) {
    throw errCode
  }
}

export const setPerson = async person => {
  try {
    return await apiPost('api/people/' + person.id, person)
  } catch(errCode) {
    throw errCode
  }
}

export const addPerson= async person => {
  try {
    return await apiPost('api/people', person)
  } catch(errCode) {
    throw errCode
  }
}

export const deletePerson = async id => {
  try {
    return await apiDelete('api/people/' + id)
  } catch(errCode) {
    throw errCode
  }
}


export const getQuestions = async () => {
  try {
    return await apiGet('api/question')
  } catch(errCode) {
    throw errCode
  }
}

export const setQuestion = async question => {
  try {
    return await apiPost('api/question/' + question.id, question)
  } catch(errCode) {
    throw errCode
  }
}

export const getQuestion = async id => {
  try {
    return await apiGet('api/question/' + id)
  } catch(errCode) {
    throw errCode
  }
}

export const addQuestion = async question => {
  try {
    return await apiPost('api/question', question)
  } catch(errCode) {
    throw errCode
  }
}

export const deleteQuestion = async id => {
  try {
    return await apiDelete('api/question/' + id)
  } catch(errCode) {
    throw errCode
  }
}

export const getQuestionImageUploadURL = async (id, key) => {
  try {
    return (await apiGet(`api/question/${id}/imageUploadURL/${key}`)).url
  } catch(errCode) {
    throw errCode
  }
}

export const uploadQuestionImage = async (id, key, file) => {
  try {
    const url = await getQuestionImageUploadURL(id, key)
    return fetch(url, {
      method: 'PUT',
      headers: { 'Content-Type': 'image/png' },
      body: file,
    })
  } catch(errCode) {
    throw errCode
  }
}

export const getShopItem = async () => {
  try {
    return await apiGet('api/shop')
  } catch(errCode) {
    throw errCode
  }
}

export const setShopItem = async item => {
  try {
    return await apiPost('api/shop/' + item.id, item)
  } catch(errCode) {
    throw errCode
  }
}

export const addShopItem= async item => {
  try {
    return await apiPost('api/shop', item)
  } catch(errCode) {
    throw errCode
  }
}

export const deleteShopItem = async id => {
  try {
    return await apiDelete('api/shop/' + id)
  } catch(errCode) {
    throw errCode
  }
}

export const getShopItemImageUploadURL = async (id) => {
  try {
    return (await apiGet(`api/question/${id}/imageUploadURL`)).url
  } catch(errCode) {
    throw errCode
  }
}

export const uploadShopItemImage = async (id, file) => {
  try {
    const url = await getQuestionImageUploadURL(id)
    return fetch(url, {
      method: 'PUT',
      headers: { 'Content-Type': 'image/png' },
      body: file,
    })
  } catch(errCode) {
    throw errCode
  }
}


export const getQuestionImageURL = (id, key) =>
  `https://s3.amazonaws.com/${AWS_BUCKET}/images/questions/${id}_${key}.png`
export const getShopItemImageURL = (id) =>
  `https://s3.amazonaws.com/${AWS_BUCKET}/images/shop/${id}.png`
