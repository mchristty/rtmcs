import React from 'react'
import styled from 'styled-components'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Input from '@material-ui/core/Input'
import { getPeople, setPerson, deletePerson, addPerson, commonErrorAlert } from '../api'

export default class extends React.Component {

  state = { people: [] }

  componentDidMount() {
    this.getPeople()
  }

  getPeople = () => getPeople().then(people => {
    this.setState({ people })
  })

  remove = id => {
    this.setState(state => ({ people: state.people.filter(person => person.id !== id) }))
  }

  onAdded = () => this.getPeople()

  render() {
    const { people } = this.state
    return (
      <PeopleContainer>
        <NewPerson onAdded={this.onAdded}/>
        <br />
        Donors & Executive Members:
        <ul>
          { people.length === 0
            ? 'There are no donors or executive members, add one using the form above.'
            : people.map(
              person => <Person key={person.id} data={person} onDelete={() => this.remove(person.id)} />
            )
          }
        </ul>
      </PeopleContainer>
    )
  }
}

const PeopleContainer = styled.div`
  font-family: Roboto, sans-serif;
  font-size: 18px;
  font-weight: lighter;
`

class Person extends React.Component {

  constructor(props) {
    super(props)
    const data = this.props.data
    this.state = {
      id: data.id,
      name: data.name,
      type: data.type,
      canSave: false,
    }
  }

  handleChange = (fieldName, ev) => {
    this.setState({ [fieldName]: ev.target.value, canSave: true })
  }

  remove = () => {
    if (window.confirm(`Remove ${this.state.name}?`)) {
      deletePerson(this.state.id)
        .then(this.props.onDelete)
        .catch(commonErrorAlert)
    }
  }

  persist = () => setPerson({
    id: this.state.id,
    type: this.state.type,
    name: this.state.name,
  })

  onSave = () => {
    this.persist()
      .then(() => {
        this.setState({ canSave: false })
      })
      .catch(commonErrorAlert)
  }

  render() {
    const { name, type, canSave } = this.state
    return (
      <PersonView
        name={name}
        type={type}
        onChange={this.handleChange}
        onRemove={this.remove}
        canSave={canSave}
        onSave={this.onSave}
      />
    )
  }
}

class NewPerson extends React.Component {

  state = {
    name: '',
    type: 'donor',
  }

  handleChange = (fieldName, ev) => {
    this.setState({ [fieldName]: ev.target.value })
  }

  add = () => {
    if (this.state.name === '') {
      alert('Please write a name')
      return
    }
    addPerson(this.state)
      .then(() => {
        this.setState({
          name: '',
          type: 'donor',
        }, () => {
          this.props.onAdded && this.props.onAdded()
        })
      })
      .catch(() => {
        commonErrorAlert()
      })
  }

  render() {
    const { name, type } = this.state
    return (
      <PersonView
        mode="add"
        name={name}
        type={type}
        onChange={this.handleChange}
        onAdd={this.add}
      />
    )
  }
}

const PersonView = ({ mode = 'edit', name, type, onChange, onAdd, onRemove, canSave = false, onSave }) => (
  <PersonContainer withTopBorder={mode === 'edit'}>
    <StyledInput value={name} onChange={ev => onChange('name', ev)} placeholder="Name" />
    <br />
    <Select
      value={type}
      onChange={ev => onChange('type', ev)}
    >
      <MenuItem value="donor">Donor</MenuItem>
      <MenuItem value="exm">Executive Member</MenuItem>
    </Select>
    {mode === 'add'
      ? <Button color="secondary" onClick={onAdd}>Add</Button>
      : <>
        <Button onClick={onRemove}>Remove</Button>
        {canSave && <Button color="secondary" onClick={onSave}>Save</Button>}
      </>
    }
  </PersonContainer>
)

const PersonContainer = styled.div`
  width: 320px;
  padding: 15px 7px;
`

const StyledInput = styled(Input)`
  width: 100%;
`
