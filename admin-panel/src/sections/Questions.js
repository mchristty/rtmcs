import React from 'react'
import styled, { css } from 'styled-components'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Input from '@material-ui/core/Input'
import Paper from '@material-ui/core/Paper'
import Chip from '@material-ui/core/Chip'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'

import {
  getQuestions,
  setQuestion,
  deleteQuestion,
  commonErrorAlert,
  addQuestion,
  uploadQuestionImage,
  getQuestion,
  getQuestionImageURL,
} from '../api'

export default class extends React.Component {

  state = { questions: [] }

  componentDidMount() {
    this.getQuestions()
  }

  getQuestions = () => getQuestions().then(questions => {
    this.setState({ questions })
  })

  remove = id => {
    this.setState(state => ({ questions: state.questions.filter(question => question.id !== id) }))
  }

  onAdded = () => this.getQuestions()

  render() {
    const { questions } = this.state
    return (
      <QuestionsContainer>
        <Question mode="add" onAdded={this.onAdded}/>
        <br />
        Dinosaur Questions:
        <ul>
          { questions.length === 0
            ? 'There are no dinosaur questions, add one using the form above.'
            : questions.map(
              question =>
                <Question
                  key={question.id}
                  data={question}
                  onDelete={() => this.remove(question.id)}
                />
            )
          }
        </ul>
      </QuestionsContainer>
    )
  }
}

const QuestionsContainer = styled.div`
  font-family: Roboto, sans-serif;
  font-size: 18px;
  font-weight: lighter;
`

class Question extends React.Component {

  constructor(props) {
    super(props)
    const data = this.props.data
    const mode = this.props.mode || 'edit'
    this.state = {
      ...(mode === 'edit') ? {
        id: data.id,
        text: data.text,
        answers: data.answers,
        correctIdx: data.correctIdx,
      } : {
        text: '',
        answers: ['Yes', 'No'],
        correctIdx: 0,
      },
      canSave: false,
      defaultImageChanged: false,
      correctImageChanged: false,
      imageHash: Math.floor(Math.random() * 10000),
    }
  }

  add = async () => {
    try {
      if (this.state.text === '') {
        alert('Please write the question text')
        return
      }
      const res = await addQuestion(this.state)
      const id = res.id
      this.setState({
        text: '',
        answers: ['Yes', 'No'],
        correctIdx: 0,
      }, () => {
        this.props.onAdded && this.props.onAdded()
      })
      if (this.state.defaultImageChanged) {
        await uploadQuestionImage(id, 'default', this.state.defaultImageChanged)
        this.setState({ defaultImageChanged: false })
      }
      if (this.state.correctImageChanged) {
        await uploadQuestionImage(id, 'correct', this.state.correctImageChanged)
        this.setState({ correctImageChanged: false })
      }
      this.setState({ imageHash: Math.floor(Math.random() * 10000) })
    } catch(err) {
      commonErrorAlert()
    }
  }

  handleChange = (fieldName, ev) => {
    this.setState({ [fieldName]: ev.target.value, canSave: true })
  }

  remove = () => {
    if (window.confirm(`Remove "${this.state.text}"?`)) {
      deleteQuestion(this.state.id)
        .then(this.props.onDelete)
        .catch(commonErrorAlert)
    }
  }

  addAnswer = answer => {
    this.setState(state => ({
      answers: [...state.answers, answer],
      canSave: true,
    }))
  }

  deleteAnswer = idx => {
    this.setState(state => ({
      answers: state.answers.filter((answer, i) => i !== idx),
      canSave: true,
    }))
  }

  persist = () => setQuestion({
    id: this.state.id,
    text: this.state.text,
    answers: this.state.answers,
    correctIdx: this.state.correctIdx,
  })

  onSave = async () => {
    try {
      await this.persist()
      if (this.state.defaultImageChanged) {
        await uploadQuestionImage(this.state.id, 'default', this.state.defaultImageChanged)
        this.setState({
          defaultImageChanged: false,
        })
      }
      if (this.state.correctImageChanged) {
        await uploadQuestionImage(this.state.id, 'correct', this.state.correctImageChanged)
        this.setState({
          correctImageChanged: false,
        })
      }
      this.setState({ canSave: false, imageHash: Math.floor(Math.random() * 10000) })
    } catch(err) {
      commonErrorAlert()
    }
  }

  onCancel = () => {
    getQuestion(this.state.id)
      .then(question => {
        this.setState({
          text: question.text,
          answers: question.answers,
          correctIdx: question.correctIdx,
          canSave: false,
          defaultImageChanged: false,
          correctImageChanged: false,
          imageHash: Math.floor(Math.random() * 10000),
        })
      })
      .catch(commonErrorAlert)
  }

  upload = (key, file) => {
    this.setState({ [`${key}ImageChanged`]: file, canSave: true })
  }

  render() {
    const {
      id,
      text,
      answers,
      correctIdx,
      canSave,
      imageHash,
      defaultImageChanged,
      correctImageChanged,
    } = this.state
    const { mode } = this.props

    return (
      <QuestionView
        mode={mode}
        id={id}
        text={text}
        imageHash={imageHash}
        answers={answers}
        correctIdx={correctIdx}
        onChange={this.handleChange}
        onRemove={this.remove}
        addAnswer={this.addAnswer}
        deleteAnswer={this.deleteAnswer}
        onAdd={this.add}
        onSave={this.onSave}
        onCancel={this.onCancel}
        upload={this.upload}
        canSave={canSave}
        defaultImageChanged={defaultImageChanged}
        correctImageChanged={correctImageChanged}
      />
    )
  }
}

class QuestionView extends React.Component {

  state = {
    newAnswerText: '',
  }

  addAnswer = () => {
    if (this.state.newAnswerText === '') {
      alert('Please write the answer text')
      return
    }
    this.props.addAnswer && this.props.addAnswer(this.state.newAnswerText)
    this.setState({ newAnswerText: '' })
  }

  newAnswerTextChanged = ev => {
    this.setState({ newAnswerText: ev.target.value })
  }

  onKeyUp = ev => {
    if (ev.keyCode === 13) {
      this.addAnswer()
    }
  }

  render () {
    const {
      mode = 'edit',
      id,
      text,
      answers,
      correctIdx,
      onChange,
      onAdd,
      onRemove,
      deleteAnswer,
      canSave,
      onCancel,
      upload,
      onSave = false,
      imageHash = '',
      defaultImageChanged,
      correctImageChanged,
    } = this.props
    const { newAnswerText } = this.state

    return (
      <QuestionContainer withTopBorder={mode === 'edit'}>
        <StyledInput value={text} onChange={ev => onChange('text', ev)} placeholder="Text" />
        <br />
        <br />
        Default image: &nbsp;
        <Button variant="contained" color={defaultImageChanged ? 'primary' : 'default'} component="label" size="small" >
          <input
            onChange={ev => upload('default', ev.target.files[0])}
            style={{ display: 'none' }}
            type="file"
            accept="image/png"
          />
          Upload &nbsp;
          <CloudUploadIcon />
        </Button>
        <div style={mode === 'edit' ? {} : { height: '14px' }}>
          {mode === 'edit' && <Img src={getQuestionImageURL(id, 'default') + '?' + imageHash} alt="default image"/>}
        </div>
        Correct image: &nbsp;
        <Button variant="contained" color={correctImageChanged ? 'primary' : 'default'} component="label" size="small" >
          <input
            onChange={ev => upload('correct', ev.target.files[0])}
            style={{ display: 'none' }}
            type="file"
            accept="image/png"
          />
          Upload &nbsp;
          <CloudUploadIcon />
        </Button>
        <div style={mode === 'edit' ? {} : { height: '14px' }}>
          {mode === 'edit' && <Img src={getQuestionImageURL(id, 'correct') + '?' + imageHash} alt="correct image"/>}
        </div>
        Add answer:
        <StyledInput value={newAnswerText} onChange={this.newAnswerTextChanged} onKeyUp={this.onKeyUp} placeholder="New Answer Text" />
        <Button color="primary" onClick={this.addAnswer}>Add answer</Button>
        <br />        
        Answers: <Answers list={answers} onDelete={deleteAnswer}/>
        Correct answer: <Select
          value={correctIdx}
          onChange={ev => onChange('correctIdx', ev)}
        >
          {answers.map((answer, idx) => (
            <MenuItem key={idx} value={idx}>{answer}</MenuItem>
          ))}
        </Select>
        <br />
        <br />
        {mode === 'add'
          ? <Button color="secondary" onClick={onAdd}>Add Question</Button>
          : <>
            <Button onClick={onRemove}>Remove</Button>
            {canSave && <>
              <Button onClick={onCancel}>Cancel</Button>
              <Button color="secondary" onClick={onSave}>Save</Button>
            </>}
          </>
        }
      </QuestionContainer>
    )
  }
}

const QuestionContainer = styled.div`
  margin: 5px;
  width: 320px;
  padding: 7px;
  ${props => props.withTopBorder && css`
    border-top: 2px solid ${props => props.theme.palette.secondary.main};
  `}
`

const StyledInput = styled(Input)`
  width: 100%;
`

const Img = styled.img`
  margin: 10px;
  width: auto;
  height: 100px;
`

const Answers = ({ list, onDelete }) => (
  <StyledPaper>
    {list.map((answer, idx) => (
      <StyledChip
        key={idx}
        label={answer}
        onDelete={() => onDelete(idx)}
      />
    ))}
  </StyledPaper>
)

const StyledPaper = styled(Paper)`
  margin: 10px 0 10px 10px;
  padding: 7px;
`

const StyledChip = styled(Chip)`
  margin: 4px;
`
