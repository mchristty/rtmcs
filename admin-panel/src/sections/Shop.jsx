import React from 'react'
import styled from 'styled-components'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Input from '@material-ui/core/Input'
import UploadImage from '@material-ui/icons/CloudUpload'
import {
    getShopItem,
    setShopItem,
    deleteShopItem,
    addShopItem,
    uploadShopItemImage,
    getShopItemImageURL,
    commonErrorAlert } from '../api'

export default class extends React.Component {

  state = { items: [] }

  componentDidMount() {
    this.getShopItem()
  }

  getShopItem = () => getShopItem().then(items => {
    this.setState({ items })
  })

  remove = id => {
    this.setState(state => ({ items: state.items.filter(item => item.id !== id) }))
  }

  onAdded = () => this.getShopItem()

  render() {
    const { items } = this.state
    return (
      <ItemsContainer>
        <NewItem mode="add" onAdded={this.onAdded}/>
        <br />
        Items:
        <ul>
          { items.length === 0
            ? 'There are no donors or executive members, add one using the form above.'
            : items.map(
                item => <Item key={item.id} data={item} onDelete={() => this.remove(item.id)} />
            )
          }
        </ul>
      </ItemsContainer>
    )
  }
}

const ItemsContainer = styled.div`
  font-family: Roboto, sans-serif;
  font-size: 18px;
  font-weight: lighter;
`

class Item extends React.Component {

  constructor(props) {
    super(props)
    const data = this.props.data
    const mode = this.props.mode || 'edit'
    this.state = {
    ...(mode === 'edit') ? {
        id: data.id,
        name: data.name
        } : {
        name: ''
        },
      id: data.id,
      name: data.name,
      image: data.image,
      canSave: false,
    }
  }

  handleChange = (fieldName, ev) => {
    this.setState({ [fieldName]: ev.target.value, canSave: true })
  }

  remove = () => {
    if (window.confirm(`Remove ${this.state.name}?`)) {
      deleteShopItem(this.state.id)
        .then(this.props.onDelete)
        .catch(commonErrorAlert)
    }
  }

  persist = () => setShopItem({
    id: this.state.id,
    image: this.state.image,
    name: this.state.name,
  })

  onSave = () => {
    this.persist()
      .then(() => {
        this.setState({ canSave: false })
      })
      .catch(commonErrorAlert)
  }

  render() {
    const { name, image, canSave } = this.state
    return (
      <ItemView
        name={name}
        image={image}
        onChange={this.handleChange}
        onRemove={this.remove}
        canSave={canSave}
        onSave={this.onSave}
      />
    )
  }
}

class NewItem extends React.Component {

  state = {
    name: '',
    image: '',
    canSave: false,
    ImageChanged: false,
    imageHash: Math.floor(Math.random() * 10000),
  }

  handleChange = (fieldName, ev) => {
    this.setState({ [fieldName]: ev.target.value })
  }

  add = async () => {
    try {
      if (this.state.name === '') {
        alert('Please write the item name')
        return
      }
      const res = await addShopItem(this.state)
      const id = res.id
      this.setState({
        name: ''
      }, () => {
        this.props.onAdded && this.props.onAdded()
      })
      if (this.state.ImageChanged) {
        await uploadShopItemImage(id, this.state.ImageChanged)
        this.setState({ ImageChanged: false })
      }
      this.setState({ imageHash: Math.floor(Math.random() * 10000) })
    } catch(err) {
      commonErrorAlert()
    }
  }


  // add = async () => {
  //       try {
  //         if (this.state.name === '') {
  //           alert('Please write the item name')
  //           return
  //         }
  //         const res = await addShopItem(this.state)
  //         const id = res.id
  //         this.setState({
  //           name: '',
  //         }, () => {
  //           this.props.onAdded && this.props.onAdded()
  //         })
  //         if (this.state.ImageChanged) {
  //           await uploadShopItemImage(id, this.state.defaultImageChanged)
  //           this.setState({ ImageChanged: false })
  //         }
  //         this.setState({ imageHash: Math.floor(Math.random() * 10000) })
  //       } catch(err) {
  //         commonErrorAlert()
  //       }



  //   // addShopItem(this.state)
  //   //   .then(() => {
  //   //     this.setState({
  //   //       name: '',
  //   //       image: '',
  //   //     }, () => {
  //   //       this.props.onAdded && this.props.onAdded()
  //   //     })
  //   //     if (this.state.ImageChanged) {
  //   //         await uploadItemImage(id, this.state.ImageChanged)
  //   //         this.setState({ImageChanged: false })
  //   //     }
  //   //     this.setState({ imageHash: Math.floor(Math.random() * 10000) })
  //   //   })
  //   //   .catch(() => {
  //   //     commonErrorAlert()
  //   //   })
  // }

  onSave = async () => {
    try {
      await this.persist()
      if (this.state.ImageChanged) {
        await uploadShopItemImage(this.state.id, this.state.ImageChanged)
        this.setState({
          ImageChanged: false,
        })
      }
      this.setState({ canSave: false, imageHash: Math.floor(Math.random() * 10000) })
    } catch(err) {
      commonErrorAlert()
    }
  }

  onCancel = () => {
    getShopItem(this.state.id)
      .then(item => {
        this.setState({
          name: item.text,
          canSave: false,
          ImageChanged: false,
          imageHash: Math.floor(Math.random() * 10000),
        })
      })
      .catch(commonErrorAlert)
  }

  upload = file => {
    this.setState({ [`ImageChanged`]: file, canSave: true })
  }

  render() {
    const { name, image, id,canSave, imageHash, ImageChanged } = this.state
    return (
      <ItemView
        mode="add"
        name={name}
        image={image}
        id={id}
        canSave={canSave}
        ImageChanged={ImageChanged}
        onChange={this.handleChange}
        imageHash={imageHash}
        onAdd={this.add}
        upload={this.upload}
        onSave={this.onSave}
      />
    )
  }
}

const ItemView = ({ mode = 'edit',id,name, image, onChange, imageHash = '',onAdd, ImageChanged, upload, onRemove, canSave = false, onSave = false }) => (
  <ItemContainer withTopBorder={mode === 'edit'}>
    <StyledInput value={name} onChange={ev => onChange('name', ev)} placeholder="Name" />
    <br />
    <br />
    Image: &nbsp;
    <Button variant="contained" color={ImageChanged ? 'primary' : 'default'} component="label" size="small" >
        <input
        onChange={ev => upload(ev.target.files[0])}
        style={{ display: 'none' }}
        type="file"
        accept="image/png"
        />
        Upload &nbsp;
        <UploadImage />
    </Button>
    <div style={mode === 'edit' ? {} : { height: '14px' }}>
        {mode === 'edit' && <Img src={getShopItemImageURL(id) + '?' + imageHash} alt="default image"/>}
    </div>
    <br/>
    {mode === 'add'
      ? <Button color="secondary" onClick={onAdd}>Add</Button>
      : <>
        <Button onClick={onRemove}>Remove</Button>
        {canSave && <Button color="secondary" onClick={onSave}>Save</Button>}
      </>
    }
  </ItemContainer>
)

const ItemContainer = styled.div`
  width: 320px;
  padding: 15px 7px;
`

const StyledInput = styled(Input)`
  width: 100%;
`
const Img = styled.img`
  margin: 10px;
  width: auto;
  height: 100px;
`