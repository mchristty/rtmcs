import FavoriteIcon from '@material-ui/icons/Favorite'
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer'
import Questions from './Questions'
import Shop from './Shop'
import Fellowship from './Fellowship'
import DonorsAndExecutiveMembers from './DonorsAndExecutiveMembers'

export default [
 {
    title: 'Dinosaur Questions',
    path: '/questions',
    content: Questions,
    icon: FavoriteIcon,
  },
  {
    title: 'Museum Shop',
    content: Shop,
    path: '/shop',
    icon: FavoriteIcon
  },
  //{
  //   title: 'Memberships',
  //   content: Memberships,
  // },
  {
    title: 'Donors & Executive Members',
    path: '/people',
    content: DonorsAndExecutiveMembers,
    icon: QuestionAnswerIcon,
  },
  // {
  //   title: 'Royal Tyrrell Museum',
  //   content: RoyalTyrrellMuseum,
  // },
  // {
  //   title: 'Royal Tyrrell Museum Cooperating Society',
  //   content: Rtmcs,
  // },
  // {
  //   title: 'Research & Science Literacy',
  //   content: ResearchAndScienceLiteracy,
  // },
  {
    title: 'Post Doctoral Fellowship',
    content: Fellowship,
    path: '/fellowship',
    icon: FavoriteIcon
  },
]
