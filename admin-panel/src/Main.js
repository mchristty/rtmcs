import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import styled from 'styled-components'
import { Route } from 'react-router-dom'
import { logout, checkAuth, refreshToken } from './api'

import SideNav from './SideNav'
import Welcome from './Welcome'
import sections from './sections'

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
}

const refreshTokenTime = 2 * 60 * 1000

class App extends React.Component {

  state = { sideNavOpen: false }

  refreshTokenInterval = -1

  componentDidMount() {
    checkAuth()
      .then(() => {
        this.refreshTokenInterval = setInterval(() => {
          refreshToken().catch(errCode => {
            if (errCode === 401) {
              this.logout()
            }
          })
        }, refreshTokenTime)
      })
      .catch(errCode => {
        if (errCode === 401) {
          this.logout()
        }
      })
  }

  openSideNav = () => {
    this.setState({ sideNavOpen: true })
  }

  onSideNavClose = () => {
    this.setState({ sideNavOpen: false })
  }

  logout = () => {
    logout()
    this.props.history.replace('/login')
  }

  componentWillUnmount() {
    if (this.refreshTokenInterval !== -1) {
      clearInterval(this.refreshTokenInterval)
    }
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.openSideNav}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              RTMCS App Admin
            </Typography>
            <Button color="inherit" onClick={this.logout}>Logout</Button>
          </Toolbar>
        </AppBar>
        <Content>
          <Route path="/" exact component={Welcome} />
          {
            sections.map(
              section => (
                <Route path={section.path} component={section.content} />
              )
            )
          }
        </Content>
        <SideNav open={this.state.sideNavOpen} onClose={this.onSideNavClose}/>
      </div>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(App)

const Content = styled.div`
  padding: 20px;
`
