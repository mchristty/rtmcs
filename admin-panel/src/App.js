import React from 'react'
import { HashRouter as Router, Redirect, Route } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { isAuth } from './api'
import { MuiThemeProvider } from '@material-ui/core'
import theme from './theme'

import Login from './Login'
import Main from './Main'

export default () => (
  <Router>
    <AppContainer>
      <Route path="/login" render={(props) =>
        isAuth()
          ? <Redirect to='/' />
          : <Login {...props} />
      }/>
      <PrivateRoute path="/" component={Main} />
    </AppContainer>
  </Router>
)

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuth()
      ? <Component {...props} />
      : <Redirect to='/login' />
  )}/>
)

const AppContainer = ({ children }) => (
  <ThemeProvider theme={theme}>
    <MuiThemeProvider theme={theme}>
      {children}
    </MuiThemeProvider>
  </ThemeProvider>
)
