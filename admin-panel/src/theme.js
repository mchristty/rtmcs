import { createMuiTheme } from '@material-ui/core/styles'

export default createMuiTheme({
  palette: {
    primary: {
      main: '#43444D',
    },
    secondary: {
      main: '#8EC449',
    },
  },
})
