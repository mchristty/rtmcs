import React from 'react'
import styled, { css } from 'styled-components'
import isDev from './isDev'

import { ViewMode } from './ViewMode'
import Modal from './Modal'
import ScreenSaver from './ScreenSaver'
import sections from './sections'
import simplePubsub from './simplePubsub'
import cms from './cms'
import CMS from './cmsContext'
import { AnimatedBlur } from './animatedBlur'

import { Questions } from './questionsContext'

const animationDuration = 400

export default class View extends React.Component {
  
  constructor(props) {
    super(props)
    this.questions = simplePubsub()
    this.cms = cms()
    this.containerRef = React.createRef()
    this.modalRef = React.createRef()
    this.windowRef = React.createRef()
    this.lastUnhandledEvent = ''
  }

  state = {
    modalOpen: false,
    modalSection: '',
  }

  componentDidMount() {
    this.windowRef.current.style.visibility = 'hidden'
    if (isDev) {
      // this.openModal('donorsAndExecutiveMembers')
    }
  }

  openModal = modalSection => {
    if (this.animationTransaction) {
      // this.lastUnhandledEvent = 'open'
      return
    }
    this.animationTransaction = true
    this.windowRef.current.style.visibility = 'hidden'
    this.setState({
      modalOpen: true,
      modalSection,
    }, () => {
      const effectName = 'modalBlurEffect' + Math.floor(Math.random() * 100000)
      this.windowRef.current.style.visibility = 'visible'
      this.blurEffect = new AnimatedBlur(effectName, this.windowRef.current, {
        steps: 4,
        duration: animationDuration,
      })
      this.blurEffect.update()
      this.windowRef.current.style.visibility = 'hidden'
      this.blurEffect.play(AnimatedBlur.BLUR_MODE.UNBLUR)
      setTimeout(() => {
        this.windowRef.current.style.visibility = 'visible'
        this.animationTransaction = false
        if (this.lastUnhandledEvent === 'close') {
          this.lastUnhandledEvent = ''
          this.closeModal()
        }
      }, animationDuration)
    })
  }

  closeModal = () => {
    if (this.animationTransaction) {
      this.lastUnhandledEvent = 'close'
      return
    }
    if (!this.state.modalOpen) {
      return
    }
    this.animationTransaction = true
    this.setState({ modalOpen: false, modalSection: '' }, () => {
      this.windowRef.current.style.visibility = 'hidden'
      this.blurEffect.play(AnimatedBlur.BLUR_MODE.BLUR)
      setTimeout(() => {
        this.questions.emit('reset')
        this.blurEffect.dispose()
        this.blurEffect = undefined
        this.animationTransaction = false
        if (this.lastUnhandledEvent === 'open') {
          this.lastUnhandledEvent = ''
          this.openModal()
        }
      }, animationDuration)
    })
  }

  render () {
    const { className, mode } = this.props
    const { modalOpen, modalSection } = this.state
    const Section = sections[modalSection] && sections[modalSection].content
    return (
      <Container className={className} mode={mode} ref={this.containerRef}>
        <ViewMode.Provider value={mode}>
          <Questions.Provider value={this.questions}>
            <CMS.Provider value={this.cms}>
              <Modal
                small={modalSection === 'royalTyrrellMuseum'}
                title={sections[modalSection] && sections[modalSection].title}
                open={modalOpen}
                onClose={this.closeModal}
                padding={this.state.modalSection !== 'rtmcs'}
                modalRef={this.modalRef}
                windowRef={this.windowRef}
              >
                {Section ? <Section key={this.state.renderCount}/> : <></>}
              </Modal>
              <ScreenSaver onActive={this.closeModal} containerRef={this.containerRef }/>
              <img className="background" src="imgs/background/main.png" alt="background" />
              <ButtonImg visible={(modalSection === '' || modalSection === 'royalTyrrellMuseum')} className="buttons" src="imgs/background/buttons/default_all.png" alt="button" />
              <ButtonImg visible={modalSection === 'museumShop'} className="buttons" src="imgs/background/buttons/selected_museum-shop.png" alt="button" />}
              <ButtonImg visible={modalSection === 'memberships'} className="buttons" src="imgs/background/buttons/selected_memberships.png" alt="button" />}
              <ButtonImg visible={modalSection === 'donorsAndExecutiveMembers'} className="buttons" src="imgs/background/buttons/selected_donors-executive-members.png" alt="button" />}
              <ButtonImg visible={modalSection === 'rtmcs'} className="buttons" src="imgs/background/buttons/selected_rtmcs.png" alt="button" />}
              <ButtonImg visible={modalSection === 'researchAndScienceLiteracy'} className="buttons" src="imgs/background/buttons/selected_research-science-literacy.png" alt="button" />}
              <ButtonImg visible={modalSection === 'postDoctoralFellowship'} className="buttons" src="imgs/background/buttons/selected_post-doctoral-fellowship.png" alt="button" />}
              <MuseumShopButton onClick={() => this.openModal('museumShop')} onTouchStart={() => this.openModal('museumShop')}/>
              <MembershipsButton onClick={() => this.openModal('memberships')} onTouchStart={() => this.openModal('memberships')}/>
              <DonorsAndExecutiveMembersButton onClick={() => this.openModal('donorsAndExecutiveMembers')} onTouchStart={() => this.openModal('donorsAndExecutiveMembers')}/>
              <RoyalTyrrellMuseumButton onClick={() => this.openModal('royalTyrrellMuseum')} onTouchStart={() => this.openModal('royalTyrrellMuseum')}/>
              <RtmcsButton onClick={() => this.openModal('rtmcs')} onTouchStart={() => this.openModal('rtmcs')}/>
              <ResearchAndScienceLiteracyButton onClick={() => this.openModal('researchAndScienceLiteracy')} onTouchStart={() => this.openModal('researchAndScienceLiteracy')}/>
              <PostDoctoralFellowshipButton onClick={() => this.openModal('postDoctoralFellowship')} onTouchStart={() => this.openModal('postDoctoralFellowship')}/>
            </CMS.Provider>
          </Questions.Provider>
        </ViewMode.Provider>
      </Container>
    )
  }
}

const Container = styled.div`
  position: relative;
  flex-shrink: 0;
  width: 1080px;
  height: 960px;
  overflow: hidden;
  .background {
    width: 100%;
    height: auto;
  }
  .buttons {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: auto;
  }
  ${props => props.mode === 'left' ? css`
    transform: translate(-60px, 60px) rotate(90deg);
  ` : props.mode === 'right' ? css`
    transform: translate(-180px, 60px) rotate(-90deg);
  ` : ''}
`

const ButtonImg = styled.img`
  visibility: ${props => props.visible ? 'visible' : 'hidden'};
`

const MuseumShopButton = styled.div`
  position: absolute;
  z-index: 200;
  left: 78px;
  top: 240px;
  width: 200px;
  height: 180px;
  `
const MembershipsButton = styled.div`
  position: absolute;
  z-index: 200;
  left: 145px;
  top: 430px;
  width: 220px;
  height: 200px;
`
const DonorsAndExecutiveMembersButton = styled.div`
  position: absolute;
  z-index: 200;
  left: 80px;
  top: 660px;
  width: 225px;
  height: 200px;
`
const RtmcsButton = styled.div`
  position: absolute;
  z-index: 200;
  left: 400px;
  top: 610px;
  width: 310px;
  height: 320px;
`
const RoyalTyrrellMuseumButton = styled.div`
  position: absolute;
  z-index: 200;
  left: 455px;
  top: 280px;
  width: 200px;
  height: 180px;
  transform: skew(-60deg, 30deg) translateY(10px) scale(0.95);
`
const ResearchAndScienceLiteracyButton = styled.div`
  position: absolute;
  left: 735px;
  top: 485px;
  width: 210px;
  height: 210px;
`
const PostDoctoralFellowshipButton = styled.div`
  position: absolute;
  z-index: 200;
  left: 800px;
  top: 260px;
  width: 210px;
  height: 200px;
`
