import React from 'react'

export const Questions = React.createContext('')

export const withQuestions = WrappedComponent => {
  return class extends React.Component {
    render() {
      const { children, ...rest } = this.props
      return (
        <Questions.Consumer>
          {questions => <WrappedComponent questions={questions} {...rest}>
            {children}
          </WrappedComponent>}
        </Questions.Consumer>
      )
    }
  }
}

export default Questions
