import React from 'react'
import styled, { css } from 'styled-components'
import palette from './palette'

export default class extends React.Component {

  onClose = ev => {
    if (!ev.defaultPrevented) {
      this.props.onClose()
    }
  }

  preventDefault = ev => {
    // avoid to close the Modal on Window events
    ev.preventDefault()
  }

  render() {
    const { open, title, children, small, padding, modalRef, windowRef } = this.props
    return (
      <Obfuscator
        id="obfuscator"
        onPointerDown={this.onClose}
        open={open}
        ref={modalRef}
      >
        <Window onPointerDown={this.preventDefault} small={small} ref={windowRef}>
          <TitleContainer>
            <Title>{title}</Title><CloseButton onClick={this.onClose} onTouchStart={this.onClose}/>
          </TitleContainer>
          <Content  padding={padding}>{children}</Content>
        </Window>
      </Obfuscator>
    )
  }
}

const Obfuscator = styled.div`
  position: absolute;
  z-index: 400;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  padding-top: 237px;
  display: flex;
  justify-content: center;
  background-color: rgba(73, 74, 82, 0.5);
  opacity: 0;
  transition: opacity linear .5s;
  pointer-events: none;
  ${props => props.open && css`
    opacity: 1;
    pointer-events: auto;
  `}
`

const Window = styled.div`
  z-index: 1001;
  width: 789px;
  height: 561px;
  background-color: white;
  border-radius: 10px;
  overflow: hidden;
  ${props => props.small && css`
    width: 500px;
    height: 250px;
  `}
`

const TitleContainer = styled.div`
  height: 65px;
  display: flex;
  align-items: center;
  background-color: ${palette.greyLighter};
`

const Title = styled.div`
  margin-top: 7px;
  width: calc(100% - 40px);
  font-size: 29px;
  letter-spacing: 0px;
  text-align: center;
  color: ${palette.red};
`

const CloseButton = props => <CloseButtonContainer {...props}>
  <CloseImg />
</CloseButtonContainer>

const CloseButtonContainer = styled.div`
  width: 40px;
  height: 60px;
  display: flex;
  align-items: center;
`

const CloseImg = styled.img.attrs({
  src: './imgs/icons/close.svg',
  alt: 'close button',
})`
  width: 20px;
  height: 20px;
`

const Content = styled.div`
  width: 100%;
  height: calc(100% - 65px);
  ${props => props.padding && css`
    padding: 41px 0 0 57px;
  `}
  color: ${palette.grey};
  font-size: 14px;
  flex-wrap:wrap;
  display: flex;
  align-items: flex-start;
`
