export default () => {
  const listeners = []

  return {
    listen: listener => {
      listeners.push(listener)
      return () => { 
        const idx = listeners.indexOf(listener)
        listeners.splice(idx, 1)
      }
    },
    emit: value => {
      for (const listener of listeners) {
        listener(value)
      }
    }
  }
}
