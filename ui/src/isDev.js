
let isDev = true

if (window.require) {
  isDev = window.require('electron-is-dev')
}

export default isDev
