import React from 'react'
import styled from 'styled-components'

import Question from '../components/Question'

export default () => (
  <>
    <img src="imgs/texts/memberships-content-v5.png" alt="membership benefits" />
    <RightContainer>
      <StyledQuestion />
      {/* <img src="imgs/membership-benefits-advice.png" alt="membership has benefits" /> */}
    </RightContainer>
  </>
)

const RightContainer = styled.div`
  margin-left: 25px; 
`

const StyledQuestion = styled(Question)`
  margin-left: 38px;
  margin-bottom: 55px;
`
