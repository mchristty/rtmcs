import React from 'react'
import styled from 'styled-components'

import Question from '../components/Question'
import SwipeableList from '../components/SwipeableList'
import palette from '../palette'
import { withCMS } from '../cmsContext'

export default withCMS(class extends React.Component {

    constructor(props) {
      super(props)
      this.state = { people: this.props.cms.getPeopleSync() }
    }

  componentDidMount() {
    this.props.cms.getPeople().then(people => {
      this.setState({ people })
    })
  }

  render () {
    return (
      <>
        <div style = {{ width: `480px` }}>
          <div>
          <img style={{ marginLeft: '5px' }} src="imgs/texts/donors-executive-members-v5.png" alt="recognizing the support of our donors" />
          </div>
          <div style={{  height: '275px', display: 'flex', alignItems: 'flex-start', overflow: 'hidden' }}>
            <div style={{ height: '100%' }}>
              {/*<ListTitle>Donors</ListTitle>*/}
              <StyledSwipeableList>
                {this.state.people.filter(person => person.type === 'donor').map((person, idx) => (
                  <Person key={idx}>{person.name}</Person>
                ))}
              </StyledSwipeableList>
            </div>
            <div style={{ height: '100%' }}>
              {/*<ListTitle>Executive Members</ListTitle>  */}                
              <StyledSwipeableList>
                {this.state.people.filter(person => person.type === 'exm').map((person, idx) => (
                  <Person key={idx}>{person.name}</Person>
                ))}
              </StyledSwipeableList>
            </div>
          </div>
        </div>
        <StyledQuestion />
      </>
    )
  }
})

const StyledQuestion = styled(Question)`
  margin-left: 50px;
`

const Person = styled.div`
  padding: 5px 5px 2px 7px;
  font-size: 16px;
  color: ${palette.red};
`

// const ListTitle = styled.div`
//   font-weight: bold;
//   font-size: 14px;
//   margin-left:25px;
// `

const StyledSwipeableList = styled(SwipeableList)`
  position: relative;
  width: 312px;
`
 