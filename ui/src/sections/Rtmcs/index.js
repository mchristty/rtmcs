import React from 'react'
import styled from 'styled-components'

import Question from '../../components/Question'
import { withViewMode } from '../../ViewMode'
import palette from '../../palette';

let isDev
if (window.require) {
  isDev = window.require('electron-is-dev')
} else {
  isDev = true
}

export default class extends React.Component {

  state = {
    progress: 0,
  }

  handleProgress = progress => {
    this.setState({ progress })
  }

  render() {
    const { progress } = this.state
    return (
      <Container>
        <Steps onProgress={this.handleProgress}/>
        <Timeline progress={progress}/>
      </Container>
    )
  }
}

const Container = styled.div`
  width: 100%;
`

const Timeline = props => <TimelineContainer>
  <Time>
    <TimelineCursor progress={props.progress} />
  </Time>
</TimelineContainer>

const TimelineContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

const Time = styled.div`
  position: relative;
  margin-top: 36px;
  width: 514px;
  height: 2px;
  background-color: ${palette.greyLighter};
`

const TimelineCursor = styled.img.attrs({
  src: 'imgs/slider/slider-UI-port-v6.png',
  alt: 'timeline cursor',
})`
  position: absolute;
  top: -17px;
  left: ${props => props.progress * 514 / 100 - 17}px;
`

const numSteps = 8
// const stepPercentage = 100 / (numSteps - 1)
const stepWidth = 735
const draggableContainerWidth = stepWidth * numSteps
const draggableContainerWidthMinusOne = stepWidth * (numSteps - 1)

const Steps = withViewMode(class extends React.Component {

  constructor(props) {
    super(props)
    this.containerRef = React.createRef()
    this.draggableContainerRef = React.createRef()
  }

  state = {
    touch: undefined,
    dragOffset: 0,
  }

  handleTouchStart = ev => {
    ev.preventDefault()
    this.cancelBoundaryAnimation()
    this.setState({ touch: ev.targetTouches[0] })
  }

  handleTouchMove = ev => {
    ev.preventDefault()
    const currentTouch = ev.targetTouches[0]
    const containerWidth = this.containerRef.current.offsetWidth
    const draggableContainerWidth = this.draggableContainerRef.current.offsetWidth
    const maxOffset = draggableContainerWidth - containerWidth
    let newDragOffset = this.state.dragOffset + getDragOffset(this.state.touch, currentTouch, this.props.mode)
    if (Math.abs(newDragOffset) > maxOffset) {
      newDragOffset = Math.sign(newDragOffset) * maxOffset
    }
    if (newDragOffset < 0) {
      newDragOffset = 0
    }

    if (this.props.onProgress) {
      this.props.onProgress(newDragOffset / draggableContainerWidthMinusOne * 100)
    }

    this.setState({
      dragOffset: newDragOffset,
      touch: currentTouch,
    })
  }

  handleTouchEnd = ev => {
    ev.preventDefault()
    this.setState({ touch: undefined })
    // this.goToBoundary()
  }

  handleMouseDown = ev => {
    // this.cancelBoundaryAnimation()
    this.setState({ touch: { clientX: ev.clientX, clientY: ev.clientY } })
    window.addEventListener('mouseup', this.handleMouseUp)
  }
  
  handleMouseMove = ev => {
    if (!this.state.touch) return
    const currentTouch = { clientX: ev.clientX, clientY: ev.clientY }
    const containerWidth = this.containerRef.current.offsetWidth
    const draggableContainerWidth = this.draggableContainerRef.current.offsetWidth
    const maxOffset = draggableContainerWidth - containerWidth
    let newDragOffset = this.state.dragOffset + getDragOffset(this.state.touch, currentTouch, this.props.mode)
    if (Math.abs(newDragOffset) > maxOffset) {
      newDragOffset = Math.sign(newDragOffset) * maxOffset
    }
    if (newDragOffset < 0) {
      newDragOffset = 0
    }
    if (this.props.onProgress) {
      this.props.onProgress(newDragOffset / draggableContainerWidthMinusOne * 100)
    }
    this.setState({
      dragOffset: newDragOffset,
      touch: currentTouch,
    })
  }

  handleMouseUp = () => {
    this.setState({ touch: undefined })
    window.removeEventListener('mouseup', this.handleMouseUp)
    // this.goToBoundary()
  }

  goToBoundary = () => {
    const closestStep = Math.round(this.state.dragOffset * numSteps / draggableContainerWidth)
    const newDragOffset = closestStep * stepWidth
    if (this.props.onProgress) {
      this.props.onProgress(newDragOffset / draggableContainerWidthMinusOne * 100)
    }
    // this.startBoundaryAnimation(newDragOffset)
  }

  startBoundaryAnimation = targetOffset => {
    this.animationStarted = true
    // this.animateBoundaryTransition(targetOffset)
  }

  cancelBoundaryAnimation = () => {
    this.animationStarted = false    
  }

  animateBoundaryTransition = targetOffset => {
    let stop = false

    let startVal = this.state.dragOffset
    let destVal = targetOffset
    let duration = 500
    let start = null
    let end = null

    const startAnimation = timeStamp => {
      start = timeStamp
      end = start + duration
      draw(timeStamp)
    }

    const draw= now => {
      if (stop || !this.animationStarted) return
      if (now - start >= duration) stop = true
      let p = (now - start) / duration
      let val = this.inOutQuad(p)
      const newDragOffset = startVal + (destVal - startVal) * val
      if (this.props.onProgress) {
        this.props.onProgress(newDragOffset / draggableContainerWidthMinusOne * 100)
      }
      this.setState({ dragOffset: newDragOffset }, () => {
        requestAnimationFrame(draw)
      })
    }

    requestAnimationFrame(startAnimation)
  }

  inOutQuad = n => {
    n *= 2;
    if (n < 1) return 0.5 * n * n;
    return - 0.5 * (--n * (n - 2) - 1);
  }

  render() {
    const { dragOffset } = this.state
    return (
      <>
      <StepsContainer
        className={this.props.className}
        {...isDev ? {
          onPointerDown: this.handleMouseDown,
          onPointerMove: this.handleMouseMove,
          onPointerUp: this.handleMouseUp,
          onTouchStart: this.handleTouchStart,
          onTouchMove: this.handleTouchMove,
          onTouchEnd: this.handleTouchEnd,
        } : {
          onTouchStart: this.handleTouchStart,
          onTouchMove: this.handleTouchMove,
          onTouchEnd: this.handleTouchEnd,
        }}
        ref={this.containerRef}
      >
      <WhiteGradient />
        <DraggableContainer
          dragOffset={dragOffset}
          ref={this.draggableContainerRef}
        >
          {/* <Step> */}
            <img style={{ marginLeft: '5px', paddingRight:'64px' }} src="imgs/sliders/RTM-Coop1-v6.png" alt="royal tyrrell museum information step 1" />

            <img style={{ marginLeft: '5px', paddingRight:'350px' }} src="imgs/sliders/RTM-Coop2-v6.png" alt="royal tyrrell museum information step 2" />
          {/* </Step> */}
          <StyledQuestion />
        </DraggableContainer>
      </StepsContainer>
      </>
    )
  }
})

function getDragOffset(touchi, touchf, mode) {
  if (mode === 'left') {
    return touchi.clientY - touchf.clientY
  } else if (mode === 'right') {
    return - touchi.clientY + touchf.clientY
  } else {
    return touchi.clientX - touchf.clientX
  }
}
// const Content = styled.div`
//   width: 100%;
//   height: calc(100% - 65px);
//   padding: 10px 0 0 0px;
//   display: flex;
//   align-items: flex-start;
// `

const StepsContainer = styled.div`
  width: ${stepWidth}px;
  padding-top: 40px;
  padding-left: 67px;
  display: flex;
  position: relative;
`
const WhiteGradient = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 80px;
  height: 101%;
  z-index:9;
  background-image: linear-gradient(to right,white, transparent);
`

const DraggableContainer = styled.div`
  margin-left: -${props => props.dragOffset}px;
  width: ${numSteps * 100+30}%;
  display: flex;
  touch-action: none;
  position:relative;
`

// const Step = styled.div`
//   width: ${stepWidth}px;
//   display: flex;
//   align-items: flex-start;
//   padding: 40px 24px 0 57px;
// `

// Step 1

const StyledQuestion = styled(Question)`
  margin-left: 10px;
  position: absolute;
  top: -26px;
  right: 45px;
`
