import React from 'react'
import styled from 'styled-components'

import Question from '../components/Question'
import Gallery from '../components/Gallery'

const items= [
  { src: 'imgs/gallery/museumshop-books1.png', link: 'Books' },
  { src: 'imgs/gallery/museumshop-apparel1.png', link: 'Apparel' },
  { src: 'imgs/gallery/museumshop-collectables1.png', link: 'Collectables' },
  { src: 'imgs/gallery/museumshop-housewares1.png', link: 'Housewares' },
  { src: 'imgs/gallery/museumshop-toys1.png', link: 'Toys' },
  // { src: 'imgs/gallery/museumshop-apparel1.png', link: 'ToysKeepsakes' },
  
]

export default () => (
  <>
    <img src="imgs/texts/museumshop-heading-text-v5.png" alt="museum shop information" />
    <Gallery items={items}/>
    <RightContainer>
      <StyledQuestion />
    </RightContainer>
  </>
)

const RightContainer = styled.div`
  margin-left: 25px; 
`

const StyledQuestion = styled(Question)`
  margin-left: 38px;
  margin-bottom: 55px;
`
