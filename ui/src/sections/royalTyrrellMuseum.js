import React from 'react'

export default () => (
  <img style={{ marginLeft: '5px' }} src="imgs/texts/RTM-heading-text-v5.png" alt="royal tyrrell museum information" />
)
