import MuseumShop from './MuseumShop'
import Memberships from './Memberships'
import DonorsAndExecutiveMembers from './DonorsAndExecutiveMembers'
import RoyalTyrrellMuseum from './RoyalTyrrellMuseum'
import Rtmcs from './Rtmcs/index'
import ResearchAndScienceLiteracy from './ResearchAndScienceLiteracy'
import PostDoctoralFellowship from './PostDoctoralFellowship'

export default {
  museumShop: {
    title: 'Museum Shop',
    content: MuseumShop,
  },
  memberships: {
    title: 'Memberships',
    content: Memberships,
  },
  donorsAndExecutiveMembers: {
    title: 'Donors & Executive Members',
    content: DonorsAndExecutiveMembers,
  },
  royalTyrrellMuseum: {
    title: 'Royal Tyrrell Museum',
    content: RoyalTyrrellMuseum,
  },
  rtmcs: {
    title: 'Royal Tyrrell Museum Cooperating Society',
    content: Rtmcs,
  },
  researchAndScienceLiteracy: {
    title: 'Research & Science Literacy',
    content: ResearchAndScienceLiteracy,
  },
  postDoctoralFellowship: {
    title: 'Post Doctoral Fellowship',
    content: PostDoctoralFellowship,
  },
}
