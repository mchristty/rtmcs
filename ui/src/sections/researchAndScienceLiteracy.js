import React from 'react'
import styled from 'styled-components'

import Question from '../components/Question'
import Gallery from '../components/Gallery'

const items= [

  { src: 'imgs/gallery/researchsciencelit-research.png', link: 'Research' },
  { src: 'imgs/gallery/researchsciencelit-collections.png', link: 'Collections' },
  { src: 'imgs/gallery/researchsciencelit-sciencelit.png', link: 'Science Literacy' },
  { src: 'imgs/gallery/researchsciencelit-exhibits.png', link: 'Exhibits' },
  // { src: 'imgs/gallery/museumshop-toys1.png', link: 'Collectables' },
  // { src: 'imgs/gallery/museumshop-apparel1.png', link: 'Toys' },
  
]

export default () => (
  <>
    <img style={{ marginLeft: '5px' }} src="imgs/texts/research-sciencelit-heading-text-v5.png" alt="membership benefits" />
    <Gallery items={items}/>
    <StyledQuestion />
  </>
)

const StyledQuestion = styled(Question)`
  margin-left: 50px;
`
//60 22 19
