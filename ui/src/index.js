import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'

ReactDOM.render(<App />, document.getElementById('root'))

// ESC key behavior
if (window.require) {
  const win = window.require('electron').remote.BrowserWindow;
  
  window.addEventListener('keyup', ev => {
    const currentWindow = win.getFocusedWindow()
    if (currentWindow.isFullScreen() && ev.keyCode === 27) {
      currentWindow.setFullScreen(false)
    }
  })
}

document.body.addEventListener('touchmove', function(event) {
  event.preventDefault()
}, false)

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

window.onwheel = preventDefault; // modern standard
window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
window.ontouchmove  = preventDefault; // mobile
document.onkeydown  = preventDefaultForScrollKeys;

