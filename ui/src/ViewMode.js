import React from 'react'

export const ViewMode = React.createContext('')

export const withViewMode = WrappedComponent => {
  return class extends React.Component {
    render() {
      const { children, ...rest } = this.props
      return (
        <ViewMode.Consumer>
          {questions => <WrappedComponent questions={questions} {...rest}>
            {children}
          </WrappedComponent>}
        </ViewMode.Consumer>
      )
    }
  }
}

export default ViewMode.Consumer
