import React from 'react'
import styled from 'styled-components'



export default class Gallery extends React.Component {
    state = { currentImage: 0}
    handleClick = (e) => {
        const index = this.props.items.findIndex(el => el.link === e.target.innerText);
        this.setState({ currentImage: index })
    }
    render() {
        return(
            <div style={{
                position: 'absolute',
                top: '201px',
                display: 'flex'
                }}>
                <LeftContainer>
                    { this.props.items.map(({src},index) =>( <Imgs src={ src } key={ index } index={ (index === this.state.currentImage) ? true : false } /> )) }

                </LeftContainer>
                <RightContainer>
                    {this.props.items.map(({link},index) => 
                    <li 
                        key={link} 
                        onClick={this.handleClick} 
                        style={(index === this.state.currentImage) ? {color: 'grey'} : {color:'red'} }
                    >
                        {link} 
                    </li>)}
                </RightContainer>
            </div>
        )
    }
}

const Imgs = ({src,index}) => <img style={{ position:"absolute", opacity:index ? 1:0 }} key={src} src={src} />

const RightContainer = styled.ul`
  margin-left: 5px; 
  font-size: 19px;
  letter-spacing: 0px;
  color: #9C0F36; 
  list-style-type:none;
  line-height:1.6;
  padding-top:84px;
`
const LeftContainer = styled.div`
    padding-top:50px
    width:250px;
    margin-left: 5px;

`
