import React from 'react'
import styled from 'styled-components'

import { withViewMode } from '../ViewMode'

export default withViewMode(class SwipeableView extends React.Component {

  constructor(props) {
    super(props)
    this.containerRef = React.createRef()
    this.scrollableContainerRef = React.createRef()
  }

  state = {
    touch: undefined,
    scrollOffset: 0,
  }

  handleTouchStart = ev => {
    console.log('handleTouchStart')
    ev.preventDefault()
    this.setState({ touch: ev.targetTouches[0] })
  }

  handleTouchMove = ev => {
    ev.preventDefault()
    const currentTouch = ev.targetTouches[0]
    const containerHeight = this.containerRef.current.offsetHeight
    const scrollableContainerHeight = this.scrollableContainerRef.current.offsetHeight
    const maxOffset = scrollableContainerHeight - containerHeight
    let newScrollOffset = this.state.scrollOffset + getScrollOffset(this.state.touch, currentTouch, this.props.mode)
    if (Math.abs(newScrollOffset) > maxOffset) {
      newScrollOffset = Math.sign(newScrollOffset) * maxOffset
    }
    if (newScrollOffset < 0) {
      newScrollOffset = 0
    }
    this.setState({
      scrollOffset: newScrollOffset,
      touch: currentTouch,
    })
  }

  handleTouchEnd = ev => {
    ev.preventDefault()
    this.setState({ touch: undefined })
  }

  handleMouseDown = ev => {
    ev.preventDefault()
    this.setState({ touch: { clientX: ev.clientX, clientY: ev.clientY } })
  }
  
  handleMouseMove = ev => {
    ev.preventDefault()
    if (!this.state.touch) return
    const currentTouch = { clientX: ev.clientX, clientY: ev.clientY }
    const containerHeight = this.containerRef.current.offsetHeight
    const scrollableContainerHeight = this.scrollableContainerRef.current.offsetHeight
    const maxOffset = scrollableContainerHeight - containerHeight
    let newScrollOffset = this.state.scrollOffset + getScrollOffset(this.state.touch, currentTouch, this.props.mode)
    if (Math.abs(newScrollOffset) > maxOffset) {
      newScrollOffset = Math.sign(newScrollOffset) * maxOffset
    }
    if (newScrollOffset < 0) {
      newScrollOffset = 0
    }
    this.setState({
      scrollOffset: newScrollOffset,
      touch: currentTouch,
    })
  }

  handleMouseUp = ev => {
    ev.preventDefault()
    this.setState({ touch: undefined })
  }

  render() {
    return <Container
      className={this.props.className}
      ref={this.containerRef}
      onPointerDown={this.handleMouseDown}
      onPointerMove={this.handleMouseMove}
      onPointerUp={this.handleMouseUp}
      onTouchStart={this.handleTouchStart}
      onTouchMove={this.handleTouchMove}
      onTouchEnd={this.handleTouchEnd}
    >
      <TopFilter />     
      <ScrollableContainer scrollOffset={this.state.scrollOffset} ref={this.scrollableContainerRef}>
        {this.props.children}
      </ScrollableContainer>
      <BottomFilter />
    </Container>
  }
})

function getScrollOffset(touchi, touchf, mode) {
  if (mode === 'left') {
    return - touchi.clientX + touchf.clientX
  } else if (mode === 'right') {
    return touchi.clientX - touchf.clientX
  } else {
    return touchi.clientY - touchf.clientY
  }
}

const Container = styled.div`
  height: 100%;
  overflow: hidden;
`

const ScrollableContainer = styled.div`
  margin-top: -${props => props.scrollOffset}px;
`

const TopFilter = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 36px;
  background-image: linear-gradient(white, transparent);
`

const BottomFilter = styled.div`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 36px;
  background-image: linear-gradient(transparent, white);
`
