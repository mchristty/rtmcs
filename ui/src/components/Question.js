import React from 'react'
import styled, { css } from 'styled-components'
import palette from '../palette'

import { withCMS } from '../cmsContext'
import { withQuestions } from '../questionsContext'

const dinosaurImages = {
  dino1: {
    default: 'imgs/dino-trivia-images-v6/dino1-default.png',
    defaultAlt: 'dinosaur 1',
    correct: 'imgs/dino-trivia-images-v6/dino1-correct-colour.png',
    correctAlt: 'dinosaur 1 correct answer',
  },
  dino2: {
    default: 'imgs/dino-trivia-images-v6/dino2-default.png',
    defaultAlt: 'dinosaur 2',
    correct: 'imgs/dino-trivia-images-v6/dino2-correct-colour.png',
    correctAlt: 'dinosaur 2 correct answer',
  },
  dino3: {
    default: 'imgs/dino-trivia-images-v6/dino3-default.png',
    defaultAlt: 'dinosaur 3',
    correct: 'imgs/dino-trivia-images-v6/dino3-correct-colour.png',
    correctAlt: 'dinosaur 3 correct answer',
  },
  dino4: {
    default: 'imgs/dino-trivia-images-v6/dino4-default.png',
    defaultAlt: 'dinosaur 4',
    correct: 'imgs/dino-trivia-images-v6/dino4-correct-colour.png',
    correctAlt: 'dinosaur 4 correct answer',
  }, 
  dino5: {
    default: 'imgs/dino-trivia-images-v6/dino5-default.png',
    defaultAlt: 'dinosaur 5',
    correct: 'imgs/dino-trivia-images-v6/dino5-correct-colour.png',
    correctAlt: 'dinosaur 5 correct answer',
  },
  dino6: {
    default: 'imgs/dino-trivia-images-v6/dino6-default.png',
    defaultAlt: 'dinosaur 6',
    correct: 'imgs/dino-trivia-images-v6/dino6-correct-colour.png',
    correctAlt: 'dinosaur 6 correct answer',
  },
}

export default withCMS(withQuestions(class extends React.Component {

  constructor(props) {
    super(props)
    const question = this.props.cms.getNextQuestionSync()
    this.state = {
      state: 'idle',
      question,
      image: dinosaurImages[question.image],
    }
  }

  componentDidMount() {
    this.unlistenReset = this.props.questions.listen(msg => {
      if (this.state.state !== 'idle' && msg === 'reset') {
        this.setState({
          state: 'idle',
          question: undefined,
        }, () => {
          this.getNextQuestion()
        })
      }
    })
  }

  getNextQuestion = () => {
    this.props.cms.getNextQuestion().then(question => {
      const image = dinosaurImages[question.image]
      this.setState({ question, image })
    })
  }

  checkAnswer = idx => {
    const { question } = this.state
    const result = question.correctIdx === idx ? 'correct' : 'wrong'
    this.setState({ state: result })
  }

  componentWillUnmount() {
    this.unlistenReset()
  }

  render () {
    const { question, image, state } = this.state

    return (
      <Container className={this.props.className}>
        {image
          ? <div style={{ position: 'relative' }}>
            <ImgContainer isCorrect={state === 'correct'}>
              <img src={image.default} alt={image.defaultAlt} />
            </ImgContainer>
            <CorrectImgContainer isCorrect={state === 'correct'} >
              <img src={image.correct} alt={image.correctAlt} />
            </CorrectImgContainer>
          </div>
          : <></>}
        {question ?
          <QuestionContainer>
            <QuestionText>
              {question.text}
            </QuestionText>
            <AnswerContainer>
              <AnswerList active={state === 'idle'}>
                {question.answers.map((answer, idx) => (
                  <React.Fragment key={idx}>
                    <Answer onClick={() => this.checkAnswer(idx)} onTouchStart={() => this.checkAnswer(idx)}>{answer}</Answer>
                    <AnswerSeparator>{idx < question.answers.length - 1 ? '|' : ''}</AnswerSeparator>
                  </React.Fragment>
                ))}
              </AnswerList>
              <CorrectResponse active={state === 'correct'}>Correct!</CorrectResponse>
              <WrongResponse active={state === 'wrong'}>
                {`The Correct Answer is '${question.answers[question.correctIdx]}'`}
              </WrongResponse>
            </AnswerContainer>
          </QuestionContainer>
        : <></>}
      </Container>
    )
  }
}))

const Container = styled.div``

const ImgContainer = styled.div`
  width: 170px;
  height: 124px;
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${palette.grey};
`

const CorrectImgContainer = styled.div`
  position: absolute;
  top: 0;
  width: 170px;
  height: 124px;
  transition: opacity linear .5s;
  opacity: ${props => props.isCorrect ? 1 : 0};
  border-radius: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${palette.grey};
`

const QuestionContainer = styled.div`
  width: 170px;
  display: flex;
  flex-direction: column;
  align-items: center;
`
  
const QuestionText = styled.div`
  position: relative;
  z-index: 10;
  margin: -6px 10px 0 10px;
  flex-shrink: 1;
  padding: 0 5px;
  height: 38px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  background-color: ${palette.blue};
  font-size: 14px;
  line-height: 1;
  text-align: center;
  color: white;
`

const AnswerContainer = styled.div`
  position: relative;
  margin-top: 3px;
  width: 140%;
`

const AnswerList = styled.span`
  opacity: 0;
  transition: opacity linear .5s;
  text-align: center;
  display: flex;
  justify-content: center;
  ${props => props.active && css`
    opacity: 1;
  `}
`

const Answer = styled.span`
  position: relative;
  z-index: 1;
  color: ${palette.green};
  margin: 0 4px;
  font-size: 14px;
  cursor: pointer;
`

const AnswerSeparator = styled.span`
  color: ${palette.grey};
`

const CorrectResponse = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  text-align: center;
  color: ${palette.green};
  opacity: 0;
  transition: opacity linear .5s;
  ${props => props.active && css`
    opacity: 1;
  `}
`

const WrongResponse = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  text-align: center;
  color: ${palette.grey};
  opacity: 0;
  transition: opacity linear .5s;
  ${props => props.active && css`
    opacity: 1;
  `}
`
