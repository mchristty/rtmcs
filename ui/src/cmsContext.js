import React from 'react'

export const CMS = React.createContext('')

export const withCMS = WrappedComponent => {
  return class extends React.Component {
    render() {
      const { children, ...rest } = this.props
      return (
        <CMS.Consumer>
          {cms => <WrappedComponent cms={cms} {...rest}>
            {children}
          </WrappedComponent>}
        </CMS.Consumer>
      )
    }
  }
}

export default CMS
