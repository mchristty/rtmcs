export default {
  grey: '#43444D',
  greyLight: '#B4B4B4',
  greyLighter: '#EAE6E0',
  blue: '#87AEE9',
  green: '#8EC449',
  red: '#9C0F36',
}
