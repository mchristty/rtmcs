import simplePubSub from '../simplePubsub'
import defaultData from './defaultData.json'

// CMS Client, Storage is S3

const remoteDataUrl = ''
const remoteDataVersionUrl = ''

const cmsDataStorageKey = 'cmsData'
const cmsDataStorageVersionKey = 'cmsDataVersion'

export default () => {
  const pubSub = simplePubSub()

  let data
  let dataVersion
  let questionCount = 0

  pubSub.listen(value => {
    data = value
  })

  const getDefaultData = () => {
    data = defaultData
  }

  const getRemoteDataVersion = () => fetch(remoteDataVersionUrl).then(r => r.json())

  const getRemoteData = async () => {
    try {
      const version = await getRemoteDataVersion()
      if (dataVersion < version) {
        const remoteData = await fetch(remoteDataUrl).then(r => r.json())
        data = remoteData
        pubSub.emit(data)
        localStorage.setItem(cmsDataStorageVersionKey, String(version))
        localStorage.setItem(cmsDataStorageKey, JSON.stringify(data))
      }
    } catch (err) {
      console.log('no internet connection')
    }
  }

  dataVersion = Number(localStorage.getItem(cmsDataStorageVersionKey) || 0)
  data = localStorage.getItem(cmsDataStorageKey)
  if (data) {
    data = JSON.parse(data)
  }

  if (dataVersion) {
    getRemoteData()
  } else {
    getDefaultData()
    getRemoteData()
  }

  async function tryToFetchRemoteData() {
    try {
      await getRemoteData()
    } catch (err) {
      console.log('no internet connection')
    }
    // wait for default data if needed
    if (!data) {
      await new Promise(resolve => {
        const unlisten = pubSub.listen(() => {
          unlisten()
          resolve()
        })
      })
    }
  }

  const getNextQuestionSync = () => {
    const numQuestions = data.questions.length
    if (questionCount >= numQuestions) {
      questionCount = 0
    }
    const question = data.questions[questionCount]
    questionCount++
    return question
  }

  return {
    listen: listener => {
      data && listener(data)
      const unlisten = pubSub.listen(listener)
      getRemoteData()
      return unlisten
    },
    getNextQuestion: async () => {
      await tryToFetchRemoteData()
      return getNextQuestionSync()
    },
    getPeople: async () => {
      await tryToFetchRemoteData()
      return data.people
    },
    getPeopleSync: () => {
      return data.people
    },
    getNextQuestionSync,
  }
}
