import React from 'react'
import styled, { css, keyframes } from 'styled-components'
import { Machine } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'

const SCREENSAVER_DELAY_MS = 90000
const SCREENSAVER_ACTIVE_TIME_MS = 5 * 60000
const SCREENSAVER_INACTIVE_TIME_MS = 60000

const machine = Machine({
  initial: 'active',
  states: {
    active: {
      initial: 'visible',
      on: { AnyActivity: 'inactive' },
      states: {
        visible: { after: { [SCREENSAVER_ACTIVE_TIME_MS]: 'hidden' } },
        hidden: { after: { [SCREENSAVER_INACTIVE_TIME_MS]: 'visible' } },
      },
    },
    inactive: {
      on: { AnyActivity: 'cancelTimer' },
      after: {
        [SCREENSAVER_DELAY_MS]: 'active',
      },
    },
    cancelTimer: {
      on: { '': 'inactive' }
    },
  }
})

export default class extends React.Component {

  constructor(props) {
    super(props)
    this.service = interpret(machine)
    this.state = { machine: machine.initialState }
    this.service.onTransition(state => {
      const active = state.matches('active')
      if (active && !this.state.machine.matches('active')) {
        this.props.onActive && this.props.onActive()
      }
      this.setState({ machine: state })
    })
  }
  
  componentDidMount() {
    this.service.start()
  }

  componentWillReceiveProps() {
    if (this.props.containerRef.current) {
      this.props.containerRef.current.addEventListener('mouseup', this.anyActivity)
      this.props.containerRef.current.addEventListener('touchstart', this.anyActivity)
    }
  }

  anyActivity = () => {
    this.service.send('AnyActivity')
  }

  componentWillUnmount() {
    this.props.containerRef.current.removeEventListener('mouseup', this.anyActivity)
    this.props.containerRef.current.removeEventListener('touchstart', this.anyActivity)
  }

  render() {
    return (
      <ScreenSaverContainer
        visible={this.state.machine.matches('active.visible')}
        onClick={this.anyActivity}
        onTouchStart={this.anyActivity}
      >
        <ScreenSaverBackground
          src="imgs/screensaver/screensaver-v6.png" 
          alt="screen saver"
        />
        <TouchAnimation />
        <TouchAnimationText>TOUCH</TouchAnimationText>
      </ScreenSaverContainer>
    )
  }
}

const ScreenSaverContainer = styled.div`
  pointer-events: none;
  position: absolute;
  z-index: 500;
  top: 0;
  left: 0;
  width: 100%;
  height: auto;
  opacity: 0;
  transition: opacity ease .5s;
  ${props => props.visible && css`
    pointer-events: auto;
    opacity: 1;
  `}
`

const ScreenSaverBackground = styled.img`
  width: 100%;
  height: auto;
`

const width = 47600
const height = 275
const steps = 136
const frameWidth = width / steps
const scale = 0.8
const scaledWidth = width * scale
const scaledFrameWidth = frameWidth * scale
const scaledHeight = height * scale

const playTouchAnimation = keyframes`
  100% { background-position: -${scaledWidth}px; }
`

const TouchAnimation = styled.div`
  position: absolute;
  left: 395px;
  top: 735px;
  width: ${scaledFrameWidth}px;
  height: ${scaledHeight}px;
  background: url('imgs/screensaver/touch-icon-sprites.png') left top;
  background-size: ${scaledWidth}px ${scaledHeight}px;
  animation: ${playTouchAnimation} 1.3s steps(${steps}) infinite;
`

const TouchAnimationText = styled.div`
  position: absolute;
  left: 490px;
  top: 885px;
  font-size: 27px;
  font-weight: bold;
  color: white;
  letter-spacing: 2px;
`
