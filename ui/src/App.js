import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import isDev from './isDev'
import './App.css'

import View from './View'

let width

const getBowserWidth = () => document.documentElement.clientWidth

if (window.require) {
  const electron = window.require('electron')
  width = electron.remote.screen.getPrimaryDisplay().workAreaSize.width
} else {
  width = getBowserWidth()
}

const browserWidth = getBowserWidth()

export default class App extends Component {

  state = {
    width: getBowserWidth(),
  }

  componentDidMount() {
    window.onresize = () => {
      this.setState({ width: getBowserWidth() })
    }
  }

  componentWillUnmount() {
    window.onresize = undefined
  }

  render() {
    return (
      isDev ? (
        <Container scaled={true} width={browserWidth}>
          <View mode=""/>
        </Container>
      ) : (
        <Container scaled={width !== 1920} width={this.state.width}>
          <View mode="left"/>
          <View mode="right"/>
        </Container>
      )
    )
  }
}

const Container = styled.div`
  display: flex;
  ${props => props.scaled && css`
    transform-origin: left top;
    transform: scale(${isDev ? 1 : props.width / 1920});
  `}
`
